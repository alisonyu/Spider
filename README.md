## Spider

**spider**是一个简单高性能的Java爬虫框架

### 功能


1.根据路径处理不同的网站内容，可以使用正则和过滤函数来判断路径

2.利用异步的Vert.x WebClient作为下载引擎，可以高效地下载网页内容

3.通过Context可以灵活地提交新的爬虫任务（url）

4.提供下载图片，写入文件，写入Json等持久化方法

5.可以定时进行爬虫任务


### Sample Code


以下通过爬取煎蛋网的图片为示例
```java
public class Sample{
	public static void main(String[] args){
        //初始化spider
        Spider spider =new Spider();
        com.alisonyu.spider.setInitUrl(getInitUri())    //设置初始的Url
                .setRouter(getRouter())    //设置路由器
                .start();                  //启动爬虫
    }

    //设置路径匹配器
    private static Router getRouter(){
        Router router=new Router();
        //route()创建一个路由并且会添加到router中
        //可以利用regex(),filter(),contentFilter()对url灵活地进行判断
        //handler是网页下载之后的处理函数，在handler中可以对内容进行持久化，获取新的链接继续爬取
        router.route().filter(url->url.contains("#comments")).handler(Sample::handleOOXX);
        //可以通过setDefaultHandler来设置当没有被任何路由匹配到的处理函数
        router.setDefaultHandler(context -> {
            System.out.println(context.getRequest().getUrl()+"没有对应的路由处理");
        });
        return router;
    }

    //设置初始的URL,在这里我们以煎蛋网为例
    private static List<String> getInitUri(){
        List<String> list=new ArrayList<>();
        String url="http://jandan.net/ooxx/page-";
        for(int i=1;i<2;i++){
            list.add(url+i+"#comments");
        }
        return list;
    }

    //包含#comments的url对应的处理函数
    private static void handleOOXX(Context context){
        //利用context可以获取JSON,HTML,byte[]等形式的下载内容
        String html=context.getResponse().getHtml();
        //我们不限定用户解析内容的方法，在实例中我们利用Jsoup来解析Html
        Document parsedHtml=Jsoup.parse(html);
        //解析获得我们需要下一步爬取的链接
        Elements pages=parsedHtml.select(".cp-pagenavi a");
        for(String link:pages.eachAttr("href")){
            //inUrlSet()用于去重，addUrl()用于把url加到爬虫队列中
            //以下即当link没有爬取过的时候把该link加入到爬虫队列中
            if(!context.inUrlSet(link)){
                context.addUrl("http:"+link);
            }
        }
        //解析获取图片链接
        Elements imgs= parsedHtml.select("#comments img");
        List<String> links=imgs.eachAttr("src");
        for(String link:links){
            System.out.println("已经爬取"+link);
            String[] ss=link.split("/");
            String fileName=ss[ss.length-1];
            //对图片进行持久化，Persistence类提供异步文件下载，写入文件等持久化函数
            Persistence.download("http:"+link,"storage/"+fileName,()->{
                System.out.println(fileName+"下载成功");
            });
        }
        //提示最好不要在该函数进行十分耗时的操作，例如IO操作等等
        //可以把耗时任务提交给该函数处理
        context.addWorkerTask(()->{
           //一些耗时操作 
        });
    }
}
```

