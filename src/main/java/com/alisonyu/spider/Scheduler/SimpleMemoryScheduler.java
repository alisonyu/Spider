package com.alisonyu.spider.Scheduler;

import com.alisonyu.spider.Downloader.Request;
import com.alisonyu.spider.Scheduler.Schedule;

import java.util.HashSet;
import java.util.LinkedList;

public class SimpleMemoryScheduler implements Schedule{

    private LinkedList<Request> requests = new LinkedList<>();
    private HashSet<String> urls = new HashSet<>();


    @Override
    public Request getRequest() {
        return requests.poll();
    }

    @Override
    public void putUrl(String url) {
        if (urls.contains(url)){
            return;
        }
        urls.add(url);
        requests.add(new Request(url));
    }

    @Override
    public void putRequest(Request request) {
        if (urls.contains(request.getUrl())){
            return;
        }
        urls.add(request.getUrl());
        requests.add(request);
    }
}
