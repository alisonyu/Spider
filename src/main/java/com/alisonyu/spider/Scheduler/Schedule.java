package com.alisonyu.spider.Scheduler;

import com.alisonyu.spider.Downloader.Request;

public interface Schedule {
    //获取URL
    public Request getRequest();
    //通过URL添加新的爬虫任务
    public void putUrl(String url);
    //通过Request添加新的爬虫任务
    public void putRequest(Request request);
    //当请求超时的时候调用
    default void onTimeOut(Request request){
        request.setRetryTime(request.getRetryTime()+1);
        putRequest(request);
    }
    //当url错误的时候或者网络错误的时候调用
    default void onError(Request request){

    }

    default boolean isEmpty(){
        return false;
    }
}
