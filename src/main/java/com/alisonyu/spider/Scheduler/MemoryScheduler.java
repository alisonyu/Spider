package com.alisonyu.spider.Scheduler;

import com.alisonyu.spider.Downloader.Request;
import io.vertx.core.impl.ConcurrentHashSet;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 单机模式的内存式URL分配器
 */
public class MemoryScheduler implements Schedule {

    //用于获取URI
    private LinkedBlockingQueue<Request> requestsQueue =new LinkedBlockingQueue<>();
    //用于去重
    private ConcurrentHashSet<String> urlSet=new ConcurrentHashSet<>();

    @Override
    public Request getRequest() {
        return requestsQueue.poll();
    }

    @Override
    public void putUrl(String url) {
        if(urlSet.contains(url)) return;
        urlSet.add(url);
        try {
            requestsQueue.put(new Request(url));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putRequest(Request request) {
        if(urlSet.contains(request.getUrl()) || request.getRetryTime()<0 ) return;
        urlSet.add(request.getUrl());
        try {
            requestsQueue.put(request);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isEmpty() {
        return requestsQueue.isEmpty();
    }
}
