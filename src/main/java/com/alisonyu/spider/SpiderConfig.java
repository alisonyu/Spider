package com.alisonyu.spider;

import com.alisonyu.spider.Downloader.Download;
import com.alisonyu.spider.Downloader.Request;
import com.alisonyu.spider.Proccessor.Process;
import com.alisonyu.spider.Router.Router;
import com.alisonyu.spider.Scheduler.Schedule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SpiderConfig {
    //是否以集群模式启动
    private boolean isCluster = false;
    //集群地址
    private String clusterAddress = "127.0.0.1";
    //集群端口
    private int clusterPort = 5800;
    //是否是客户端模式
    private boolean isClient = true;
    //REDIS设置
    private String redisHost = "127.0.0.1";
    private int redisPort = 6379;
    private String redisEncoding = "UTF-8";
    private boolean redisTcpKeepAlive = true;
    private boolean tcpNoDelay = true;
    private String redisUrlSetKey = "spiderUrlSet";
    private String redisUrlQueueKey = "spiderUrlQueue";
    //爬取间隔（毫秒）
    private int Interval = 200;
    //同时处理多少条信息
    private int instanceAmount = 1;
    //最大重试次数（默认不重试）
    private int maxRetryTime = 0;
    //爬虫处理router
    private Router router = new Router();
    //爬虫初始的URLS
    private List<String> initUrls = new ArrayList<>();
    private List<Request> initRequests = new ArrayList<>();
    //设置下载器，默认使用VertxDownloader
    private Download downloader;
    //设置处理器,默认使用DefaultProcess
    private Process processor;
    //设置调度器，单机模式默认使用MemoryScheduler
    private Schedule scheduler;

    public boolean isCluster() {
        return isCluster;
    }

    public void setCluster(boolean cluster) {
        isCluster = cluster;
    }

    public String getClusterAddress() {
        return clusterAddress;
    }

    public void setClusterAddress(String clusterAddress) {
        this.clusterAddress = clusterAddress;
    }

    public int getClusterPort() {
        return clusterPort;
    }

    public void setClusterPort(int clusterPort) {
        this.clusterPort = clusterPort;
    }

    public int getInterval() {
        return Interval;
    }

    public void setInterval(int interval) {
        Interval = interval;
    }

    public Router getRouter() {
        return router;
    }

    public void setRouter(Router router) {
        this.router = router;
    }

    public boolean isClient() {
        return isClient;
    }

    public void setClient(boolean client) {
        isClient = client;
    }

    public Download getDownloader() {
        return downloader;
    }

    public void setDownloader(Download downloader) {
        this.downloader = downloader;
    }

    public Process getProcessor() {
        return processor;
    }

    public void setProcessor(Process processor) {
        this.processor = processor;
    }

    public Schedule getScheduler() {
        return scheduler;
    }

    public void setScheduler(Schedule scheduler) {
        this.scheduler = scheduler;
    }

    public List<String> getInitUrls() {
        return initUrls;
    }

    public int getInstanceAmount() {
        return instanceAmount;
    }

    public void setInstanceAmount(int instance) {
        this.instanceAmount = instance;
    }

    public void setInitUrls(List<String> initUrls) {
        if (this.initUrls == null){
            this.initUrls = initUrls;
        }else{
            this.initUrls.addAll(initUrls);
        }

    }

    public void setInitUrls(String[] urls){
        if (this.initUrls==null){
            this.initUrls = new ArrayList<>();
        }
        initUrls.addAll(Arrays.asList(urls));
    }

    public List<Request> getInitRequests() {
        return initRequests;
    }

    public void setInitRequests(List<Request> initRequests) {
        this.initRequests = initRequests;
    }

    public int getMaxRetryTime() {
        return maxRetryTime;
    }

    public void setMaxRetryTime(int maxRetryTime) {
        this.maxRetryTime = maxRetryTime;
    }

    public void clear(){
        this.initRequests.clear();
        this.initUrls.clear();
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public String getRedisEncoding() {
        return redisEncoding;
    }

    public void setRedisEncoding(String redisEncoding) {
        this.redisEncoding = redisEncoding;
    }

    public boolean isRedisTcpKeepAlive() {
        return redisTcpKeepAlive;
    }

    public void setRedisTcpKeepAlive(boolean redisTcpKeepAlive) {
        this.redisTcpKeepAlive = redisTcpKeepAlive;
    }

    public boolean isTcpNoDelay() {
        return tcpNoDelay;
    }

    public void setTcpNoDelay(boolean tcpNoDelay) {
        this.tcpNoDelay = tcpNoDelay;
    }

    public String getRedisUrlSetKey() {
        return redisUrlSetKey;
    }

    public void setRedisUrlSetKey(String redisUrlSetKey) {
        this.redisUrlSetKey = redisUrlSetKey;
    }

    public String getRedisUrlQueueKey() {
        return redisUrlQueueKey;
    }

    public void setRedisUrlQueueKey(String redisUrlQueueKey) {
        this.redisUrlQueueKey = redisUrlQueueKey;
    }
}
