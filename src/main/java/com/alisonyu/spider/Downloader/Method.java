package com.alisonyu.spider.Downloader;

/**
 * Created by yu on 2017/9/25.
 */
public enum Method {
    GET("get"),
    POST("post"),
    PUT("put"),
    DELETE("delete");

    private Method(String name){
        this.value = name;
    }
    private String value;

    public String getValue(){
        return this.value;
    }

    public static Method getMethod(String value){
        if (value.equals(GET.value)) return GET;
        if (value.equals(POST.value)) return POST;
        if (value.equals(PUT.value)) return PUT;
        if (value.equals(DELETE.value)) return DELETE;
        return GET;
    }
}
