package com.alisonyu.spider.Downloader;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class HttpResponse {
    private byte[] bytes;
    private String content;
    private int statusCode;
    private boolean success;

    public HttpResponse(){}

    public HttpResponse(String content,int statusCode){
        this.content = content;
        this.statusCode = statusCode;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getContent(){
        return content;
    }

    /**
     * 支持转换编码为对应字符串
     * @param encoding 编码类型
     * @return 对应编码的字符串
     * @throws UnsupportedEncodingException
     */
    public String getString(String encoding) throws UnsupportedEncodingException {
        return new String(getBytes(),encoding);
    }

    public int getStatusCode(){
        return statusCode;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
