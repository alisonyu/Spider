package com.alisonyu.spider.Downloader;

import io.vertx.core.json.JsonObject;

/**
 * 请求包装类,允许失败重试
 * Created by yu on 2017/9/25.
 */
public class Request
{
    private String url;
    private Method method;
    private int retryTime = 0; //重试次数

    public Request(String url){
        setUrl(url);
        //默认使用get方法获取
        setMethod(Method.GET);
    }

    public Request(String url,Method method){

        setUrl(url);
        setMethod(method);
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }


    public int getRetryTime() {
        return retryTime;
    }

    public void setRetryTime(int retryTime) {
        this.retryTime = retryTime;
    }

    public JsonObject toJsonObject(){
        JsonObject json = new JsonObject();
        json.put("url",url);
        json.put("method",method.getValue());
        return json;
    }

    public static Request toRequest(JsonObject jsonObject){
        String url = jsonObject.getString("url");
        String method = jsonObject.getString("method");
        Method m;
        if (url==null) return null;
        if (method == null){
            m=Method.GET;
        }
        else {
            m=Method.getMethod(method);
        }
        return new Request(url,m);
    }
}
