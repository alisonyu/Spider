package com.alisonyu.spider.Downloader;

import java.util.concurrent.CompletableFuture;

public interface Download {
    CompletableFuture<HttpResponse> asyncDownload(Request request);

    HttpResponse download(Request request);

    void setMaxRetryTime(int k);
}
