package com.alisonyu.spider.Router;

import com.alisonyu.spider.Proccessor.Context;

import java.util.function.Consumer;

/**
 * Created by yu on 2017/9/24.
 */
public interface RouteHandler
{
    public Consumer handler(Consumer<Context> consumer);
}
