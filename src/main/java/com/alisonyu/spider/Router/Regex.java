package com.alisonyu.spider.Router;

/**
 * Created by yu on 2017/9/24.
 */
public interface Regex {
    public Filter regex(String regex);
}
