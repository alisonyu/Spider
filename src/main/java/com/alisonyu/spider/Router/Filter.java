package com.alisonyu.spider.Router;

import java.util.function.Predicate;

/**
 * Created by yu on 2017/9/22.
 */
public interface Filter {
    RouteHandler contentFilter(Predicate<String> predicate);
    RouteHandler filter(Predicate<String> predicate);
}
