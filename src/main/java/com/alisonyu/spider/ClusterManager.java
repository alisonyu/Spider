package com.alisonyu.spider;

import com.alisonyu.spider.cluster.ClusterServerVerticle;
import io.vertx.core.Vertx;

public class ClusterManager {

    private SpiderFactory factory;

    public ClusterManager(){
        this(new SpiderConfig());
    }

    public ClusterManager(SpiderConfig config){
        config.setClient(false);
        config.setCluster(true);
        this.factory = new SpiderFactory(config);
    }

    public void start(){
        Vertx vertx = factory.getVertx();
        vertx.deployVerticle(new ClusterServerVerticle(factory.getScheduler()));
    }
}
