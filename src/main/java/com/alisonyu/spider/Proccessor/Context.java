package com.alisonyu.spider.Proccessor;

import com.alisonyu.spider.Downloader.HttpResponse;
import com.alisonyu.spider.Downloader.Request;
import com.alisonyu.spider.Spider;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * 爬虫结果返回context，用于获取爬虫结果以及与爬虫引擎沟通
 * Created by yu on 2017/9/23.
 */
public class Context implements ContextInterface{
    private Request request;
    private HttpResponse response;
    private Spider spider;


    public Context(HttpResponse response,Spider spider){
        this.response=response;
        this.spider=spider;
    }

    public Context(Request request,HttpResponse response,Spider spider){
        this.response=response;
        this.request=request;
        this.spider=spider;
    }

    @Override
    public void addUrl(String url) {
        spider.addToTaskQueue(url);
    }

    public void addWorkerTask(Runnable runnable){
        this.spider.handleTask(runnable);
    }

    public Future addWorkerTask(Callable callable){
        return this.spider.handleTask(callable);
    }

    public HttpResponse getResponse(){
        return this.response;
    }

    public Request getRequest(){
        return this.request;
    }


}
