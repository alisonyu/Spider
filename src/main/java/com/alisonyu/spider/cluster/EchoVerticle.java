package com.alisonyu.spider.cluster;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;

public class EchoVerticle extends AbstractVerticle{

    @Override
    public void start() throws Exception {
        EventBus eb = vertx.eventBus();
        eb.<String>consumer("echo",message -> {
           String content = message.body();
           System.out.println("I have received: "+content);
           message.reply(content);
        });
        System.out.println("ECHO VERTICLE启动成功");
    }
}
