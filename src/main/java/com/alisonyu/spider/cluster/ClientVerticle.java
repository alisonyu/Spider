package com.alisonyu.spider.cluster;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.util.concurrent.CountDownLatch;

public class ClientVerticle {
    public static void main(String[] args) throws InterruptedException {
        Config hazelcastConfig = new Config(); //创建hazelcast配置
        JoinConfig joinConfig = hazelcastConfig.getNetworkConfig().getJoin();
        joinConfig.getTcpIpConfig().addMember("127.0.0.1:5800").setEnabled(true);
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getAwsConfig().setEnabled(false);
        ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);
        VertxOptions options = new VertxOptions().setClusterManager(mgr);
        CountDownLatch latch = new CountDownLatch(1);
        Vertx.clusteredVertx(options, res->{
            if (res.succeeded()){
                Vertx vertx = res.result();
                EventBus eb = vertx.eventBus();
                eb.<String>send("echo","hello world",message->{
                    if (message.succeeded()){
                        String reply = message.result().body();
                        System.out.println(reply);
                    }else{
                        System.out.println("error");
                    }
                    latch.countDown();
                });
            }
        });
        latch.await();
    }

}
