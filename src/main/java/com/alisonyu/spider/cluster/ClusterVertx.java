package com.alisonyu.spider.cluster;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class ClusterVertx {

    public void start(){

        Config hazelcastConfig = new Config(); //创建hazelcast配置
        hazelcastConfig.getNetworkConfig().setPort(5800).setPublicAddress("localhost");
        JoinConfig joinConfig = hazelcastConfig.getNetworkConfig().getJoin();
        joinConfig.getTcpIpConfig().setEnabled(true);
        joinConfig.getMulticastConfig().setEnabled(false);
        ClusterManager mgr = new HazelcastClusterManager(hazelcastConfig);
        VertxOptions options = new VertxOptions().setClusterManager(mgr);
        Vertx.clusteredVertx(options,res->{
            if (res.succeeded()){
                Vertx vertx = res.result();
                vertx.deployVerticle(new ClusterServerVerticle());
            }
        });
    }

    public static void main(String[] args){
        ClusterVertx clusterVertx = new ClusterVertx();
        clusterVertx.start();
    }

}
