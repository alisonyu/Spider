package example;

import com.alisonyu.spider.ClusterManager;
import com.alisonyu.spider.Scheduler.RedisScheduler;
import com.alisonyu.spider.SpiderConfig;

public class ClusterServerExample {
    public static void main(String[] args){
        SpiderConfig config = new SpiderConfig();
        config.setScheduler(new RedisScheduler());
        ClusterManager manager = new ClusterManager(config);
        manager.start();
    }
}
