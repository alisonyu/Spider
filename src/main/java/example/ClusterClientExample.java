package example;

import com.alisonyu.spider.Router.Router;
import com.alisonyu.spider.Spider;
import com.alisonyu.spider.SpiderConfig;

import java.util.ArrayList;
import java.util.List;

public class ClusterClientExample {
    public static void main(String[] args){
        SpiderConfig config = new SpiderConfig();
        config.setCluster(true);
        config.setRouter(getRouter());
        config.setInitUrls(getUrl());
        config.setInterval(3);
        Spider spider = new Spider(config);
        spider.start();
    }

    public static Router getRouter(){
        Router router = new Router();
        router.route().filter(url->url.contains("jandan.net/ooxx")).handler(context -> {
            System.out.println(context.getResponse().getContent());
        });
        return router;
    }

    public static List<String> getUrl(){
        List<String> list = new ArrayList<>();
        String baseUrl = "http://jandan.net/ooxx/page-";
        for(int i=1;i<50;i++){
            list.add(baseUrl+i);
        }
        return list;
    }
}
