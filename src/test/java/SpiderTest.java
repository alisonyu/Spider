import com.alisonyu.spider.Router.Router;
import com.alisonyu.spider.Scheduler.RedisScheduler;
import com.alisonyu.spider.Spider;
import com.alisonyu.spider.SpiderConfig;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SpiderTest {

    @Test
    public void fun() throws InterruptedException {
        SpiderConfig config = new SpiderConfig();
        config.setRouter(getRouter());
        config.setInitUrls(getUrl());
        config.setScheduler(new RedisScheduler());
        Spider spider = new Spider(config);
        spider.start();
        Thread.sleep(100000);
    }

    public Router getRouter(){
        Router router = new Router();
        router.route().filter(url->url.contains("jandan.net/ooxx")).handler(context -> {
            //System.out.println(context.getResponse().getContent());
            System.out.println("成功处理"+context.getRequest().getUrl());
        });
        return router;
    }

    public List<String> getUrl(){
        List<String> list = new ArrayList<>();
        String baseUrl = "http://jandan.net/ooxx/page-";
        for (int i=1;i<=10;i++){
            list.add(baseUrl+i);
        }
        for (int i=1;i<=10;i++){
            list.add(baseUrl+i);
        }
        return list;
    }

}
