package com.alisonyu.spider.Downloader;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class BrowserUtilTest {

    @Test
    public void getChrome() {
        WebDriver driver = BrowserUtil.getChrome();
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        for (int i=1200;i<1210;i++){
            long t1 = System.currentTimeMillis();
            driver.get("https://drug.supfree.net/qq.asp?id="+i);
            System.out.println("耗时（秒）："+TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()-t1));
        }
        //System.out.println(driver.getPageSource());
        driver.close();
    }
}